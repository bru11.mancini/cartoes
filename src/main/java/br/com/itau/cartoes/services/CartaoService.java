package br.com.itau.cartoes.services;

import br.com.itau.cartoes.models.Cartao;
import br.com.itau.cartoes.models.Cliente;
import br.com.itau.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CartaoService {
    @Autowired
    CartaoRepository cartaoRepository;

    @Autowired
    ClienteService clienteService;

    public Cartao cadastrarCartao(int idCliente, Cartao cartao){
        Cliente cliente = clienteService.buscarCliente(idCliente);
        cartao.setAtivo(false);
        cartao.setCliente(cliente);
        Cartao cartaoObjeto = cartaoRepository.save(cartao);
        return cartaoObjeto;
    }

    public Cartao buscarCartao(int idCartao){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(idCartao);
        if (optionalCartao.isPresent()){
            return optionalCartao.get();
        }
        throw new RuntimeException("O cartão não foi encontrado!");
    }

    public Cartao mudarStatusCartao(int idCartao, boolean statusCartao){
        Cartao cartao = buscarCartao(idCartao);
        cartao.setId(idCartao);
        cartao.setAtivo(statusCartao);
        Cartao cartaoObjeto = cartaoRepository.save(cartao);
        return cartaoObjeto;
    }


}
