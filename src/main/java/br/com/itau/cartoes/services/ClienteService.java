package br.com.itau.cartoes.services;

import br.com.itau.cartoes.models.Cliente;
import br.com.itau.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente salvarCliente(Cliente cliente){
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public Cliente buscarCliente(int idCliente){
        Optional<Cliente> optionalCliente = clienteRepository.findById(idCliente);
        if (optionalCliente.isPresent()){
            return optionalCliente.get();
        }
        throw new RuntimeException("O cliente não foi encontrado!");
    }

}
