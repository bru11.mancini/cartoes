package br.com.itau.cartoes.services;

import br.com.itau.cartoes.models.Cartao;
import br.com.itau.cartoes.models.Cliente;
import br.com.itau.cartoes.models.Pagamento;
import br.com.itau.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    PagamentoRepository pagamentoRepository;

    @Autowired
    CartaoService cartaoService;

    @Autowired
    ClienteService clienteService;

    public Pagamento efetuarPagamento(int idCartao, Pagamento pagamento){
        Cartao cartaoObjeto = cartaoService.buscarCartao(idCartao);
        pagamento.setCartao(cartaoObjeto);
        Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
        return pagamentoObjeto;
    }

    public List<Pagamento> buscarPagamentoPorIdCartao(int idCartao){
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllByCartao_Id(idCartao);
        return (List) pagamentos;
    }
}
