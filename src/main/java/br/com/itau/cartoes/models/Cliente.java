package br.com.itau.cartoes.models;

import javax.persistence.*;

@Entity
public class Cliente {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int clienteId;

    private String name;

//    @ManyToOne(cascade = CascadeType.ALL)
//    private Cartao cartao;

    public Cliente() {
    }

    public Cliente(int clienteId, String name) {
        this.clienteId = clienteId;
        this.name = name;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public Cartao getCartao() {
//        return cartao;
//    }
//
//    public void setCartao(Cartao cartao) {
//        this.cartao = cartao;
//    }
//
//    public Cliente(int idCliente, String nomeCliente, Cartao cartao) {
//        this.idCliente = idCliente;
//        this.nomeCliente = nomeCliente;
//        this.cartao = cartao;
//    }


}
