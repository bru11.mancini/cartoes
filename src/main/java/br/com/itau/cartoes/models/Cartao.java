package br.com.itau.cartoes.models;

import javax.persistence.*;

@Entity
public class Cartao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private int numero;

    private boolean ativo;

    @ManyToOne(cascade = CascadeType.ALL)
    private Cliente cliente;

//    @ManyToOne(cascade = CascadeType.ALL)
//    private Pagamento pagamento;

    public Cartao() {
    }

    public Cartao(int id, int numero, boolean ativo) {
        this.id = id;
        this.numero = numero;
        this.ativo = ativo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

//    public Cartao(Pagamento pagamento) {
//        this.pagamento = pagamento;
//    }
//
//
//    public Pagamento getPagamento() {
//        return pagamento;
//    }
//
//    public void setPagamento(Pagamento pagamento) {
//        this.pagamento = pagamento;
//    }


    public Cartao(Cliente cliente) {
        this.cliente = cliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
}
