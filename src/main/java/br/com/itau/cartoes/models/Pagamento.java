package br.com.itau.cartoes.models;

import javax.persistence.*;

@Entity
public class Pagamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idPagamento;

    private String descricao;

    private double valor;

    @ManyToOne(cascade = CascadeType.ALL)
    private Cartao cartao;

    public Pagamento() {
    }



    public Pagamento(int idPagamento, String descricao, double valor) {
        this.idPagamento = idPagamento;
        this.descricao = descricao;
        this.valor = valor;
    }

    public int getIdPagamento() {
        return idPagamento;
    }

    public void setIdPagamento(int idPagamento) {
        this.idPagamento = idPagamento;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Pagamento(Cartao cartao) {
        this.cartao = cartao;
    }

    public Cartao getCartao() {
        return cartao;
    }

    public void setCartao(Cartao cartao) {
        this.cartao = cartao;
    }
}
