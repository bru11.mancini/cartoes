package br.com.itau.cartoes.repositories;

import br.com.itau.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByCartao_Id(Integer cartao_id);

}
