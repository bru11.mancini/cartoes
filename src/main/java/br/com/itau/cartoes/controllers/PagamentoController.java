package br.com.itau.cartoes.controllers;

import br.com.itau.cartoes.DTO.PagamentoDTO;
import br.com.itau.cartoes.models.Pagamento;
import br.com.itau.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
//@RequestMapping("/pagamento")
@RequestMapping()
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;




    @PostMapping("/pagamento/")
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento efetuarPagamento(@RequestBody PagamentoDTO pagamentoDTO){
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(pagamentoDTO.getDescricao());
        pagamento.setValor(pagamentoDTO.getValor());
        return pagamentoService.efetuarPagamento(pagamentoDTO.getCartao_id(), pagamento);
    }

    @GetMapping("/pagamentos/{idCartao}")
    public List<Pagamento> exibirPagamentos (@PathVariable(name = "idCartao", required = false) Integer idCartao){
        try {
            List<Pagamento> pagamentos = pagamentoService.buscarPagamentoPorIdCartao(idCartao);
            return pagamentos;
        } catch (RuntimeException exception){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

}
